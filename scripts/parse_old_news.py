# -*- coding: utf-8 -*-
import os
import transliterate
import string
import selenium
import selenium.webdriver
from bs4 import BeautifulSoup

os.system("rm -rf ../content/images/news/*")
os.system("rm -rf ../content/news/*")

NEWS_LISTS = [
    ('https://graphics.cs.msu.ru/en/front?page=', 0, 5),
    ('https://graphics.cs.msu.ru/ru/news?page=', 0, 13),
]
NEWS_LIST_URL_PREFIX = 'https://graphics.cs.msu.ru/en/front?page='


def geturl(prev_url):
    if (prev_url[0] == '/'):
        return 'https://graphics.cs.msu.ru' + prev_url
    else:
        return prev_url


options = selenium.webdriver.chrome.options.Options()
options.add_argument("--headless")
driver = selenium.webdriver.Chrome('./chromedriver', options=options)

news_dates, news_urls = [], []
for news_list_url, page_start, page_end in NEWS_LISTS:
    for news_list_page in range(page_start, page_end + 1):
        driver.get(news_list_url + str(news_list_page))
        cur_dates = driver.find_elements_by_xpath("//td[@class='views-field views-field-created']")
        cur_urls = driver.find_elements_by_xpath("//td[@class='views-field views-field-view-node']")
        cur_dates = [elem.text.strip() for elem in cur_dates]
        cur_urls = [elem.find_elements_by_tag_name('a')[0].get_attribute("href") for elem in cur_urls]
        cur_dates = ["20" + elem[6:8] + "-" + elem[3:5] + "-" + elem[0:2] for elem in cur_dates]
        news_dates.extend(cur_dates)
        news_urls.extend(cur_urls)

for cur_news_id, (news_date, url) in enumerate(sorted(zip(news_dates, news_urls))):
    driver.get(url)
    data = driver.find_elements_by_xpath("//div[@class='left-corner']")[0]
    header_text = data.find_elements_by_tag_name('h2')[0].text
    header_text = ' '.join(header_text.strip().split())
    data = data.find_elements_by_xpath(".//div[@class='content clear-block']")[0]
    soup = BeautifulSoup(data.get_attribute('innerHTML'), features="html.parser")
    new_image_names_preffix = transliterate.translit(header_text.lower(), 'ru', reversed=True)
    new_image_names_preffix = new_image_names_preffix.translate(str.maketrans('', '', string.punctuation))
    new_image_names_preffix = '_'.join(new_image_names_preffix.split()[:5])
    images_in_soup_cnt = len(list(soup.select('img')))
    cur_img_id = 0
    md_content = []
    md_content.append("Active: yes\n" + \
                      "Title: " + header_text + "\n" + \
                      "Category: news\n" + \
                      "Date: " + news_date + " 00:00\n" + \
                      "Id: news_number_" + str(cur_news_id + 1)
                      )
    has_bad_tag = False
    for elem in soup.find_all(recursive=False):
        if elem.name == 'center':
            elem.name = 'p'
        if elem.name == 'p':
            for img in elem.select('img'):
                new_image_path = new_image_names_preffix
                if images_in_soup_cnt >= 2:
                    new_image_path = new_image_names_preffix + "_" + str(cur_img_id)
                new_image_path = "/images/news/" + new_image_path + img.attrs['src'][-4:]
                mdimg = "![" + header_text + "](" + new_image_path + " \"" + header_text + "\")"
                os.system("wget -q -O ../content" + new_image_path + " " + geturl(img.attrs['src']))
                cur_img_id += 1
                img.insert_after(mdimg)
                img.extract()
            for a in elem.select('a'):
                if 'staff' in a.attrs['href']:
                    print("STAFF", a.attrs['href'], " in news with id", cur_news_id + 1)
                mda = "[" + a.text.strip() + "](" + a.attrs['href'] + ")"
                a.insert_after(mda)
                a.extract()
            md_content.append(elem.text.strip())
        elif elem.name == 'img':
            new_image_path = new_image_names_preffix
            if images_in_soup_cnt >= 2:
                new_image_path = new_image_names_preffix + "_" + str(cur_img_id)
            new_image_path = "/images/news/" + new_image_path + elem.attrs['src'][-4:]
            new_elem = "![" + header_text + "](" + new_image_path + " \"" + header_text + "\")"
            os.system("wget -q -O ../content" + new_image_path + " " + geturl(elem.attrs['src']))
            cur_img_id += 1
            md_content.append(new_elem)
        elif elem.name == 'iframe':
            new_elem = "{% youtube " + elem.attrs['src'].split('/')[-1] + \
                       " [" + elem.attrs['width'] + \
                       "] [" + elem.attrs['height'] + "] %}"
            md_content.append(new_elem)
        elif elem.name == 'a':
            if 'staff' in elem.attrs['href']:
                print("STAFF", elem.attrs['href'], " in news with id", cur_news_id + 1)
            new_elem = "[" + elem.text.strip() + "](" + elem.attrs['href'] + ")"
            md_content.append(new_elem)
        elif elem.name == 'li':
            new_elem = '- ' + elem.text.strip()
            md_content.append(new_elem)
        elif elem.name == 'ul':
            new_elem = []
            for node in elem.select('li'):
                new_elem.append('- ' + node.text.strip())
            new_elem = '\n\n'.join(new_elem)
            md_content.append(new_elem)
        elif elem.name == 'ol':
            new_elem = []
            for i, node in enumerate(elem.select('li')):
                new_elem.append(str(i + 1) + '. ' + node.text.strip())
            new_elem = '\n\n'.join(new_elem)
            md_content.append(new_elem)
        else:
            print("BAD TAG IN:", url)
            has_bad_tag = True
    if has_bad_tag:
        md_content = [md_content[0], soup.text.strip()]
        print("WROTE NEWS WITH BAD CONTENT IN:", "news_" + str(cur_news_id + 1) + ".md", "from", url)
    md_content = "\n\n".join(md_content) + "\n"
    with open("../content/news/news_" + str(cur_news_id + 1) + ".md", "w") as fp:
        fp.write(md_content)
driver.quit()

'''
Bad links: 10, 13, 26, 31, 41, 42, 46, 47, 95, 96, 105, 111
'''
