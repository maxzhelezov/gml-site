Active: yes
Title: Summer School on the Internet of Things 2016
Category: news
Date: 2016-03-16 00:00

The annual Microsoft Research Summer School will take place in Kazan, Russia, from July 17 to 24, 2016. Qualified candidates should complete their applications and submit them by April 18, 2016. The topic of this school is Internet of Things. Besides introductory courses, a wide range of issues will be covered, from user experience design and adoption of the application's behavior depending on the data from a set of different sensors to the specifics of designing applications suitable for energy-efficient and wireless signal interference robustness scenarios. The programme is designed for students, PhD students and young researchers. It will include lectures and practical sessions by researchers from leading universities and research organizations from all over the world. See more information on the [school website](http://research.microsoft.com/en-us/events/ssiot/).
