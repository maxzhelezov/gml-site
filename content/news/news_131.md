Active: yes
Title: IC3D Best Paper Award 2016 en
Category: news
Date: 2017-01-07 00:00

The article "Multilayer semi-transparent edge processing for depth image based rendering" by [Erofeev Mikhail]({filename}/people/mikhail_erofeev.md) was recognized as best at the [International Conference on 3D Imaging](http://www.3dstereomedia.eu/ic3d) in Liege, Belgium. 
The work presents the first of its kind multi layer algorithm of video matting, which allows to significantly improve precision of processing non-trivial scenes  by the possibility of processing object overlaps in camera projection. Congratulations to Mikhail.

![IC3D Best Paper Award 2016]({static}/images/news/ic3d_best_paper_award_2016_0.jpg "IC3D Best Paper Award 2016")

![IC3D Best Paper Award 2016]({static}/images/news/ic3d_best_paper_award_2016_1.jpg "IC3D Best Paper Award 2016")
