Active: yes
Title: Results of the project “3D films without pain” were widely discussed in mass media
Category: news
Date: 2016-05-23 00:00

For the recent 8 years our lab carries out research on stereoscopic films’ quality
analysis and improvement. In February 2016
head of video group Dr. Vatolin gave a talk at the oldest and the
largest conference on stereoscopic imagery — [Stereoscopic Displays and
Applications Conference](http://www.stereoscopic.org/2016/index.html) in San-Francisco. The talk was devoted to unique and interesting
project. For the first time ever [105  Blu-Ray films were analysed with 10
quality metrics](http://compression.ru/video/vqmt3d/).
The analysis revealed main industry trends.Thousands of scenes that can
potentially cause headache were found. 100+ stereoscopic movies quality
analysis results were discussed.

{% youtube UVe9DVuxqGg [560] [315] %}

Many
Russian and international mass media wrote about this talk and VQMT3D project
(more than 250 notes were published, including but not limited to: 29 Spanish,
6 Dutch, 2 Finish, 2 Vietnamien, 2 Polish, Portuguese, Indonesian, Ukrainian, Bulgarian). For
example: [TASS](http://tass.ru/nauka/2705365), [Lenta.ru](https://lenta.ru/news/2016/02/29/3dfilms/), [Komsomolskaya Pravda](http://www.kp.ru/daily/26502/3370291/), [Paris Guardian](http://www.parisguardian.com/index.php/sid/241822541), [Republica de las Ideas](http://www.republica.com/2016/03/01/detectan-por-que-algunas-peliculas-en-3d-pueden-provocar-dolor-de-cabeza/). The extended text was
published by [“Science and Life”](https://www.nkj.ru/news/28284/).
