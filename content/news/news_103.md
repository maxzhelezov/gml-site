Active: yes
Title: Denis Zobnin was awarded a diploma for the talk "Brain MRI registration algorithm based on key points matching"
Category: news
Date: 2015-01-28 00:00

Denis Zobnin, a student from our laboratory, was awarded a diploma for the talk "Brain MRI registration algorithm based on key points matching" (authors O.V. Senyukova, D.S. Zobnin, A.V. Petraikin) on the International Conference on Artificial Neural Networks [Neuroinformatics-2015](http://neuroinfo.ru/index.php/en/). Congratulations on the first scientific success!
