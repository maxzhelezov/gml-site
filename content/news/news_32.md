Active: yes
Title: Microsoft Сomputer Vision School
Category: news
Date: 2011-04-14 00:00

Microsoft Сomputer Vision School (MCVS) will take place in Moscow, Russia, on July 28 – August 3, 2011. The school is sponsored by Microsoft Research and organized in cooperation with Lomonosov Moscow State University. Participation is free. We welcome you to register before April 30, 2011.
