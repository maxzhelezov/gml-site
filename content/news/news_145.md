Active: yes
Title: Our students won "Evrika" competition
Category: news
Date: 2018-01-04 00:00

A CMC students team won "Evrika" competition in the IT nomination. The final of the competition was held on 19th December, totally 25 teams took place in the competition.

The team members:

- Zvezdakov Sergei (master student)
- Antsiverova Anastasiya (master student)
- Kondranin Denis (bachelor student)

[News on the competition website](http://kpi-eureka.ru/news_20122017).

Congrat Sergei, Anastasiya and Denis!

![CMC students won "Evrika" competition in the IT nomination]({static}/images/news/studenty_vmk_stali_pobediteljami_konkursa.jpg "CMC students won "Evrika" competition in the IT nomination")
