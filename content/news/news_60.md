Active: yes
Title: Congratulations to winners of course work competition in CMC MSU!
Category: news
Date: 2012-10-31 00:00

Three students of Graphics & Media Lab have recieved prizes in course work competition in our department of MSU:

- Andrey Rybintsev (2nd place)
- Alexey Fedorov (2nd place)
- Vyacheslav Napadovskiy (3rd place)

We congratulate our winners!
