Active: yes
Title: Defense of Ph.D. thesis of Victor Gaganov
Category: news
Date: 2011-05-19 00:00

Defense of Ph.D. thesis of Victor Gaganov will take place on May 31th, 2011, at 11 a.m. in conference room of Keldsyh Institute of Applied Mathemathics(you can [download thesis](http://keldysh.ru/council/1/gaganov.pdf) in russian).
