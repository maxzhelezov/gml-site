Active: yes
Title: Collaboration with Microsoft Research Cambridge
Category: news
Date: 2010-06-16 00:00

We are pleased to announce the beginning of collaboration of Graphics&Media Lab's Vision Group with [Microsoft Research Cambridge](http://research.microsoft.com). Our collaborators in MSRC are [Pushmeet Kohli](http://research.microsoft.com/en-us/um/people/pkohli/) and [Carsten Rother](http://research.microsoft.com/en-us/people/carrot/). We wish the collaboration to be fruitful and result in good publications and new technologies.
