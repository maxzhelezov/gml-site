Active: yes
Title: PCL and Trimble Code Sprint 2012 en
Category: news
Date: 2012-01-12 00:00

Sergey Ushakov and Alexander Velizhev won the competition for participation in development of Point Cloud Library ([http://pointclouds.org](http://pointclouds.org)) within Code Sprint 2012, sponsored by Trimble company. The Point Cloud Library (PCL) is a standalone, large scale, open source project for 3D point cloud processing. Sergey and Alexander will implement and integrate into the library two modern algorithms for 3D point cloud segmentation.
