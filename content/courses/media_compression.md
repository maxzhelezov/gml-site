Active: yes
Category: courses
Id: media_compression
Title: Media compression
Image: images/missing_image.jpg
Teachers: dmitriy_vatolin
CourseOrder: 3

This course takes part in autumn semester and is about different compression algorithms for various mediums such as text, photo, and video. Algorithms that are introduced in the course include both classical and modern machine learning approaches. Lectures are supplemented with tasks in C/C++ and Python so attendees can utilize gained knowledge in practice. Approximately top 5 students with the best performance are taken to the lab on a trial basis. More information in telegram channel [@VGCourse](https://t.me/vgcourse)