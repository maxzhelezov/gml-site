Active: yes
Category: courses
Id: video_processing
Title: Video processing
Image: images/missing_image.jpg
Teachers: dmitriy_vatolin
CourseOrder: 5

Course takes place in the spring semester and is devoted to different video processing algorithms that include scene change detector, deinterlacer, 4D video processing, supper resolutions. Course gives intro into video processing, machine learning and deep learning. Lectures are supplemented with tasks in Python so attendees can utilize gained knowledge in practice. We are taking circa 10 students (including ones from the autumn semester). Students with the best performance have high chances of applying to the lab. More information in telegram channel [@VGCourse](https://t.me/vgcourse)