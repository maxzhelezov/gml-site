Active: yes
Title: How to join
Category: admission
Id: admission

# How to join

## Bachelor students

Currently only bachelor students from [CMC MSU](https://cs.msu.ru/en) may join our lab.
Our lab has four groups:

* computer vision ([Anton Konushin]({filename}/people/anton_konushin.md), [Vlad Shakhuro]({filename}/people/vlad_shakhuro.md))
* video processing ([Dmitriy Vatolin]({filename}/people/dmitriy_vatolin.md))
* computer graphics ([Vladimir Frolov]({filename}/people/vladimir_frolov.md))
* medical image processing ([Olga Senyukova]({filename}/people/olga_senyukova.md))

Every April each group chooses a small number (from 2 to 5) of interested
students out of second year bachelor students (precise number of students
varies from year to year). Two most important selection criteria are GPA (our
students usually have GPA > 4) and strong results either in
[course]({static}/courses.html) or in practical project. If you would
like to join our lab, please don't wait for April, start in September. Write a
letter to prospective scientific advisors, know actual admission requirements
(they may vary from advisor to advisor), participate in courses and projects.

## Master students

Our lab is one of the organizers of "Computer vision, graphics and image
processing" master program. Details of the program (in russian) are published
on the [CMC MSU integrated master program
site](http://master.cmc.msu.ru/?q=ru/node/3316). Strong math and programming
skills, willingness to conduct research are required to successfully finish the
program. Contact prospective scientific advisor before applying for program.

## PhD students

Please contact prospective scientific advisor to know whether there are
vacations for PhD students in his or her group.
