#!/usr/bin/env python
# -*- coding: utf-8 -*- #

import os
import datetime
AUTHOR = 'Graphics and Media Lab'
SITENAME = 'Graphics and Media Lab'
OUTPUT_PATH = 'public'
PATH = 'content'
CURRENT_YEAR = str(datetime.datetime.now().year)

SITEURL = ''
if os.getenv('GITLAB_CI') is not None:
    SITEURL = 'https://graphics.cs.msu.ru'

TIMEZONE = 'Europe/Moscow'
DEFAULT_DATE = '2021-01-01'
DEFAULT_LANG = 'en'

MENUITEMS = [
    ('Home', SITEURL + '/home.html'),
    ('People', SITEURL + '/people.html'),
    ('Projects', SITEURL + '/projects.html'),
    ('Publications', SITEURL + '/publications.html'),
    ('Courses', SITEURL + '/courses.html'),
    ('How to join', SITEURL + '/pages/admission.html'),
]

FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None
DISPLAY_PAGES_ON_MENU = False
DISPLAY_CATEGORIES_ON_MENU = False
DIRECT_TEMPLATES = []
PAGINATED_TEMPLATES = {}
ARTICLE_EXCLUDES = ['page']
STATIC_PATHS = [
    'images',
    'publications/pdf'
]

FILENAME_METADATA = '(?P<title>.*)'
POSSIBLE_LANGS = ['en']
CATEGORY_SAVE_AS = ''
AUTHOR_SAVE_AS = ''
PAGINATION_PATTERNS = (
    (1, '{base_name}.html', '{base_name}.html'),
    (2, '{base_name}/page_{number}.html', '{base_name}/page_{number}.html'),
)
ARTICLE_URL = '{category}/{slug}.html'
ARTICLE_SAVE_AS = '{category}/{slug}.html'
PAGE_URL = 'pages/{id}.html'
PAGE_SAVE_AS = 'pages/{id}.html'
TEMPLATE_PAGES = {
    'home.html': 'index.html',
}

PATH_TO_MISSING_ID_TO_PEOPLE_INFO = './content/missing_people_ids.csv'
PATH_TO_MISSING_ID_TO_PROJECTS_INFO = './content/missing_projects_ids.csv'
PUBLICATIONS_INFO_PATH = './content/publications/publications_info.json'
PUBLICATIONS_BIB_PATH = './content/publications/publications_bibtexs.bib'
SELCETED_PUBLICATIONS_PATH = 'content/publications/selected_publications.json'
LAB_GROUPS_META_INFO = [
    {
        'group_id': 'cv_group',
        'group_title': 'Computer vision group',
    },
    {
        'group_id': 'video_group',
        'group_title': 'Video processing group'
    },
    {
        'group_id': 'computer_graphics_group',
        'group_title': 'Computer graphics group'
    },
    {
        'group_id': 'biomedical_group',
        'group_title': 'Biomedical image analysis group'
    }
]

PLUGIN_PATHS = [".", "plugins"]
PLUGINS = [
    "site_data_processor",
    "render_math",
    "better_codeblock_line_numbering",
    "my_markdown_formatting",
    "parse_toc",
    "my_youtube_plugin"
]

# Plugins configs
MATH_JAX = {
    'color': 'inherit',
    'align': 'center'
}
TOC = {
    'TOC_HEADERS': '^h[1-6]',
    'TOC_RUN': 'true',
    'TOC_INCLUDE_TITLE': 'false',
}
