# Как обновить публикации

Сама информация о публикациях содержится в папке `./content/publications`.

Сначала нужно подробно разобрать файлы, которые содержатся в этой папке.
- Файл `publications_bibtexs.bib` - содежит `.bib`-представления всех публикаций.
- Файл `publications_info.json` - содежит всю информацию о каждой из публикации, которая будет парситься при генерации сайта.
- Файл `selected_publications.json` - содержит список избранных публикаций.
- Папка `pdf` - для хранения `pdf`-текстов публикаций.

## Добавление `pdf`-текстов:
Тексты `pdf` публикаций могут храниться как на самом сайте, так и в виде ссылок на внешние ресурсы.
Чтобы добавить ссылку нужно отредактировать поле `pdf_path` публикации в `publications_info.json`.
- Если публикация сохранена локально на сайте, то ее нужно сохранить в папке `./content/publications/pdf/`.
  Ссылка `pdf_path` на нее будет иметь вид `"{static}/publications/pdf/filename.pdf"`.
- Если публикация на внешнем ресурсе, то ссылка `pdf_path` на нее будет иметь вид `"http://..."`.

## Автоматическое обновление публикаций.

Для автоматического обновления публикаций нужно:
1. Зайти в папку `./scripts`.
2. Создать файл `istina_username_password.txt` и записать в первой строчке свой логин в [Истине](https://istina.msu.ru/), а во второй строчке пароль.
3. Скачать [chromedriver](https://sites.google.com/a/chromium.org/chromedriver/downloads) версии, которая совпадает с версией Google Chrome, установленной на компьютере. Положить его в эту папку с именем `chromedriver`.
4. Запустить скрипт `parse_istina_script.py`. Он должен отрабатывать, если запускать его без параметров. Это скрипт посетит страницы людей в Истине и выкачает `.bib`-файлы со статьями оттуда, занесет информацию из них в `publications_bibtexs.bib` и `publications_info.json`. Скрипт может работать от 3 до 30 минут в зависимости от того, как быстро будут подгружаться страницы из Истины.
5. Посмотреть на `git diff` в файлах `publications_bibtexs.bib` и `publications_info.json`. При необходимости скорректировать данные о новых статьях.

При обновлении используются метаданные, указанные в информации о людях.
А именно для каждого из `.md`-файлов в `./content/people/`:
- `ParseNewPublications` - нужно ли парсить публикации этого человека. Содержит либо `yes`, либо `no`.
- `id` - это `id` человека. Возможные id людей должны быть перечислены в [документе](./docs/people_list.md).
- `Surname`, `PublicationsPossibleSurnames` - это встречающиеся в публикациях написания фамилии человека.
- `Name`, `PublicationsPossibleNames` - это встречающиеся в публикациях написания имени человека.
- `IstinaPage` - это ссылка на страницу в [Истине](https://istina.msu.ru/) человека. Может отсутствовать, тогда при автоматическом обновлении публикаций не будет просматриваться страница этого человека.
- `LabGroups` - это список групп лабораторий, к которым нужно будет относить публикации человека.

## Обновление из `.bib`-файла

Если в Истине нет публикации, но известен её `.bib`-файл, то можно обновить публикации скриптом `parse_publications.py`.
Для этого надо в параметре `--update_bib_path` передать путь до `.bib`-файла с информацией, в параметре `--update_group_id` передать название группы лаборатории, к которой относится публикация. Остальные параметры должны работать со стандартными значениями.

## Ручное обновление

Можно вручную добавить информацию о публикации в файлы `publications_bibtexs.bib` и `publications_info.json`. Но тогда надо не забыть правильно расставить `id` людей.
