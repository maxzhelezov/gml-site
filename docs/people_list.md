## Id и имена людей (для быстрого редактирования связей):

Если человека в списке нет, то инструкция по быстрому добавлению человека находится в [документе](./docs/add_person.md).

Для автоматического обновления списка нужно:
1. Зайти в папку `scripts`.
2. Запустить скрипт `update_docs_ids.py`

| Id | Имя человека |
|---|---|
| dmitriy_vatolin | Dmitriy Vatolin |
| anton_konushin | Anton Konushin |
| vladimir_frolov | Vladimir Frolov |
| olga_senyukova | Olga Senyukova |
| vlad_shakhuro | Vlad Shakhuro |
| mikhail_erofeev | Mikhail Erofeev |
| olga_barinova | Olga Barinova |
| anna_sokolova | Anna Sokolova |
| denis_zobnin | Denis Zobnin |
| vadim_sanzharov | Vadim Sanzharov |
| dmitriy_kulikov | Dmitriy Kulikov |
